<?php

  include 'header.inc.php';
?>

	<div id="container">


<form class="form-horizontal" name="form1" method="post" action="registrervare.php">
  <fieldset>
    <legend>Legg til vare</legend>
	<div class="form-group">
      <label for="inputNavn" class="col-lg-2 control-label">Navn</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputNavn" name="navn" placeholder="Navn">
      </div>
    </div>
	<div class="form-group">
      <label for="inputAntall" class="col-lg-2 control-label">Antall</label>
      <div class="col-lg-10">
        <input type="number" class="form-control" id="inputAntall" name="antall" min="0" step="1" value="1">
      </div>
    </div>


    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <button type="reset" class="btn btn-default">Cancel</button>
        <button  type="submit" name="Submit" value="Registrer" class="btn btn-primary">Registrer</button>
      </div>
    </div>
  </fieldset>
</form>

<?php 
    include 'footer.inc.php';
?>