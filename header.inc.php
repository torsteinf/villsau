<html>

<head>
    <title>Villsaubutikkens lager</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="bootstrap.css" title="main" />
    <link rel="stylesheet" type="text/css" href="bootstrap-formhelpers.min.css" title="main" />

</head>

<body>

    <div class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="index.php" class="navbar-brand">Villsaubutikkens lager</a>
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            </div>
            <div class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <li><a href="endringslogg.php">Endringslogg</a></li>
                </ul>
            </div>
        </div>
    </div>
