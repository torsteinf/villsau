CREATE DATABASE Villsaubutikken CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE Lager (LNr int AUTO_INCREMENT, Navn varchar(30), Antall int, Sistendret datetime NOT NULL DEFAULT NOW(), CONSTRAINT LNrPN PRIMARY KEY(LNr)) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

INSERT INTO `Lager` (`Navn`, `Antall`) VALUES('Pølse', 25);

// user: user
// pw: password

CREATE TABLE Lager_history LIKE Lager;

ALTER TABLE Lager_history MODIFY COLUMN LNr int(11) NOT NULL,
DROP PRIMARY KEY, ENGINE = MyISAM, ADD action varchar(8) DEFAULT 'insert' FIRST,
ADD revision INT(6) NOT NULL AUTO_INCREMENT after action,
ADD dt_datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER revision,
ADD PRIMARY KEY (LNr, revision);

DROP TRIGGER IF EXISTS MyDB.data__ai;
DROP TRIGGER IF EXISTS MyDB.data__au;
DROP TRIGGER IF EXISTS MyDB.data__bd;

CREATE TRIGGER MyDB.data__ai AFTER INSERT ON Lager FOR EACH ROW
    INSERT INTO Lager_history SELECT 'insert', NULL, NOW(), d.*
    FROM Lager AS d WHERE d.LNr = NEW.LNr;

CREATE TRIGGER MyDB.data__au AFTER UPDATE ON Lager FOR EACH ROW
    INSERT INTO Lager_history SELECT 'update', NULL, NOW(), d.*
    FROM Lager AS d WHERE d.LNr = NEW.LNr;

CREATE TRIGGER MyDB.data__bd BEFORE DELETE ON Lager FOR EACH ROW
    INSERT INTO Lager_history SELECT 'delete', NULL, NOW(), d.*
    FROM Lager AS d WHERE d.LNr = OLD.LNr;
